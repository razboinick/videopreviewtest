<?php

require_once 'vendor/autoload.php';
require_once 'pages/SearchVideoPage.php';
require_once 'pages/SearchResultsPage.php';

class VideoPreviewTest extends PHPUnit_Extensions_Selenium2TestCase
{
    /**
     * Setting up the browser
     */
    public function setUp()
    {
        $this->setBrowser('chrome');
        $this->setBrowserUrl('https://yandex.ru/');
    }

    /**
     * @test
     */
    function previewCheck()
    {
        //Search the video
        $page = new SearchVideoPage($this);
        $videoSearchpage = $page->inputVideoName('ураган');
        $videoResPage = $videoSearchpage->searchButtonClick();

        //Remember the css of preview container
        $videoResPage ->rememberOldCssOfPreviewContainer();

        //Move mouse to the first video
        $videoResPage->moveMouseToTheFirstVideo();

        //Wait for preview css changes
        $videoResPage->waitForCssOfPreviewChanges();

        //Check that video is playing
        $videoResPage->assertCssPreviewContainerIsCorrect();
    }

    /**
     * Stop the webdriver
     */
    public function tearDown()
    {
        $this->stop();
    }
}