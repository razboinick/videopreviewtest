<?php

/**
 * Class SearchResultsPage
 * class of page with elements for video search results
 */
class SearchResultsPage
{
    /**
     * SearchResultsPage constructor.
     * @param $testContext
     */
    public function __construct($testContext)
    {
        $this->testContext = $testContext;
        $this->firstVideo = $this->_waitAndFind('serp-item_pos_0');
        $this->previewElement = $this->testContext->byXPath('//div[contains(@class,\'serp-item_pos_0\')]//div[@class=\'thumb-image__preview thumb-preview__target\']');
        $this->oldCss = null;
        $this->cssClassWeNeedToHave = 'thumb-image__preview thumb-preview__target thumb-preview__target_playing';
    }

    /**
     * Moves mouse to the first video of video search result list
     */
    public function moveMouseToTheFirstVideo()
    {
        $this->testContext->moveto($this->firstVideo);
    }

    /**
     * Remember css value of first video preview container
     */
    public function rememberOldCssOfPreviewContainer()
    {
        $this->oldCss = $this->previewElement->attribute('class');
    }

    /**
     * Wait for css changes twice.
     * Need to wait twice due to css changes
     * first from
     * "thumb-image__preview thumb-preview__target"
     * to "thumb-image__preview thumb-preview__target thumb-preview__target_loading"
     * and then to
     * "thumb-image__preview thumb-preview__target thumb-preview__target_playing"
     */
    public function waitForCssOfPreviewChanges()
    {
        $this->_waitCssChanged($this->previewElement, $this->oldCss);

        //remember css again
        $this->rememberOldCssOfPreviewContainer();
        $this->_waitCssChanged($this->previewElement, $this->oldCss);
    }

    /**
     * Assert that css value of first video preview container
     * is equal to the css when preview is playing
     */
    public function assertCssPreviewContainerIsCorrect()
    {
        $newCssClass = $this->previewElement->attribute('class');
        $this->testContext->assertEquals($this->cssClassWeNeedToHave, $newCssClass);
    }

    /**
     * Wait for element exists
     * @param $selector
     * @return mixed
     */
    protected function _waitAndFind($selector)
    {
        $element = $this->testContext->waitUntil(function ($testCase) use ($selector) {
            try {
                $element = $this->testContext->byClassName($selector);
                if ($element->displayed()) {
                    return $element;
                }
            } catch (PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e) {
            }
        }, 5000);
        return $element;
    }

    /**
     * Wait for css changes
     * @param $element
     * @param $oldCss
     * @return mixed
     */
    protected function _waitCssChanged($element, $oldCss)
    {
        $element = $this->testContext->waitUntil(function () use ($element, $oldCss) {
            try {
                if ($element->attribute('class') != $oldCss) {
                    return $element;
                }
            } catch (PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e) {
            }
        }, 5000);
        return $element;
    }
}