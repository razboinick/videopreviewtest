<?php

require_once 'SearchResultsPage.php';

/**
 * Class SearchVideoPage
 * class of page with elements for video search
 */
class SearchVideoPage
{
    /**
     * SearchVideoPage constructor.
     * @param $testContext
     */
    public function __construct($testContext)
    {
        $testContext->url('video/');
        $this->searchInput = $testContext->byClassName('input__control');
        $this->searchButton = $testContext->byClassName('search2__button');
        $this->testContext = $testContext;
    }

    /**
     * Input video name and wait for the suggestion popup
     * @param $value
     * @return $this
     */
    public function inputVideoName($value)
    {
        $this->searchInput->value($value);

        //Need for suggesstion popup appears
        $this->_waitAndFind('suggest2__content');
        return $this;
    }

    /**
     * Click the seaarch button and return the page of search results
     * @return SearchResultsPage
     */
    public function searchButtonClick()
    {
        $this->searchButton->click();
        return new SearchResultsPage($this->testContext);
    }

    /**
     * Wait for element exists for 3 seconds
     * @param $selector
     * @return mixed
     */
    protected function _waitAndFind($selector)
    {
        $element = $this->testContext->waitUntil(function ($testCase) use ($selector) {
            try {
                $element = $this->testContext->byClassName($selector);
                if ($element->displayed()) {
                    return $element;
                }
            } catch (PHPUnit_Extensions_Selenium2TestCase_WebDriverException $e) {
            }
        }, 3000);
        return $element;
    }

}